package main

import (
	"context"
	authPb "github.com/envoyproxy/go-control-plane/envoy/service/auth/v2"
	"github.com/gogo/googleapis/google/rpc"
	"google.golang.org/grpc"
	"testing"
	"time"
)

func makeRequest(token string) *authPb.CheckRequest {
	return &authPb.CheckRequest{
		Attributes: &authPb.AttributeContext{
			Request: &authPb.AttributeContext_Request{
				Http: &authPb.AttributeContext_HttpRequest{
					Headers: map[string]string{
						"authorization": token,
					},
				},
			},
		},
	}
}

func TestCheck_InvalidResponse(t *testing.T) {
	tokens := []string{"Bearer", "large-token"}
	for _, token := range tokens {
		req := makeRequest(token)
		service := new(authorizationService)
		resp, err := service.Check(context.Background(), req)
		if err != nil {
			t.Fatalf("shouldn't got error: %v", err)
		}

		if resp.Status.Code != int32(rpc.UNAUTHENTICATED) {
			t.Fatalf("should return UNAUTHENTICATED error (got %v != %v)", resp.Status.Code, rpc.UNAUTHENTICATED)
		}
	}
}

func TestCheck_ValidResponse(t *testing.T) {
	token := "Bearer foo=bar"
	req := makeRequest(token)
	service := new(authorizationService)
	resp, err := service.Check(context.Background(), req)
	if err != nil {
		t.Fatalf("shouldn't got error: %v", err)
	}

	if resp.Status.Code != int32(rpc.OK) {
		t.Fatalf("should return OK status error (got %v != %v)", resp.Status.Code, rpc.OK)
	}

	if len(resp.GetOkResponse().Headers) == 0 {
		t.Error("should set x-ext-auth-* header")
	}
}

func TestGrpcServer(t *testing.T) {
	s := new(server)

	go func() {
		err := s.start(":0")
		if err != nil {
			t.Fatalf("failed to serve: %v", err)
		}
	}()

	<-time.After(time.Millisecond * 100) // waiting for listener ready
	conn, err := grpc.Dial(s.listener.Addr().String(), grpc.WithInsecure())
	if err != nil {
		t.Fatalf("failed to dial server")
	}
	client := authPb.NewAuthorizationClient(conn)
	ctx := context.Background()
	rsp, err := client.Check(ctx, makeRequest("Bearer foo"))
	if err != nil {
		t.Fatalf("failed to call server: %v", err)
	}
	if rsp.GetStatus().Code != int32(rpc.OK) {
		t.Fatalf("error return (got %v != %v)", rsp.GetStatus().Code, rpc.OK)
	}

	s.grpcServer.Stop()
}
